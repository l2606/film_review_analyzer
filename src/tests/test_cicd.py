from cgitb import reset
from locale import currency
import sys, os
from unittest import result

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../')

import cicd 

def test_add():
    result = cicd.add(3, 5)
    expected = 8
    
    assert result == expected

def test_substract():
    result = cicd.substract(10, 5)
    expected = 5

    assert result == expected


def test_multiply():
    result = cicd.multiply(4, 5)
    expected = 20

    assert result == expected



